<?php

/**
 * @file
 * Admin settings for the Shoppingads module.
 */

/**
 * Admin settings form for the Shoppingads module
 */
function shoppingads_admin_settings() {
  $form = array();
  $form['shoppingads'] = array(
    '#type' => 'fieldset',
    '#title' => 'shoppingads '. t('settings'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE
  );
  
  $form['shoppingads']['shoppingads_publisher_id'] = array(
    '#type' => 'textfield',
    '#title' => t('Publisher ID'),
    '#default_value' => variable_get('shoppingads_publisher_id', ''),
    '#size' => 30,
    '#maxlength' => 30,
    '#required' => TRUE,
    '#description' => t('Enter your shoppingads.com publisher ID.')
  );
  
  $form['shoppingads']['shoppingads_ad_campaign'] = array(
    '#type' => 'textfield',
    '#title' => t('Campaign ID'),
    '#default_value' => variable_get('shoppingads_ad_campaign', 'default'),
    '#size' => 40,
    '#maxlength' => 40,
    '#description' => t('Enter an shoppingads.com campaign ID (optional).')
  );
  
  $form['shoppingads']['shoppingads_ad_format'] = array(
    '#type' => 'select',
    '#title' => t('Ad Format'),
    '#default_value' => variable_get('shoppingads_ad_format', ''),
    '#options' => shoppingads_ad_formats(),
    '#required' => TRUE,
    '#description' => t('The Ad format you want to use in your nodes.')
  );

  $form['shoppingads']['shoppingads_ad_attitude'] = array(
    '#type' => 'select',
    '#title' => t('Attitude Selection'),
    '#default_value' => variable_get('shoppingads_ad_attitude', ''),
    '#options' => array(
        'true' => 'Classic',
        'false' => 'Basic',
        'cool' => 'Cool Blue',
        'fader' => 'Ad Fader',
        'etched' => 'Etched'
        ),
    '#required' => TRUE,
    '#description' => t('The Ad style.')
  );

  $form['shoppingads']['shoppingads_weight'] = array(
    '#type' => 'select',
    '#title' => t('Weight'),
    '#default_value' => variable_get('shoppingads_weight', 0),
    '#options' => drupal_map_assoc(range(-20, 20)),
    '#description' => t('Specifies the position of the ad. A low weight, e.g. <strong>-20</strong> will display the ad above the content and a high weight, e.g. <strong>20</strong> below the content.')
  );
  
  $form['shoppingads']['shoppingads_new_window'] = array(
    '#type' => 'checkbox',
    '#title' => t('New window'),
    '#default_value' => variable_get('shoppingads_new_window', ''),
    '#description' => t('Check this to open ad links in a new window.')
  );
  
  $form['shoppingads']['shoppingads_teaser_full_view'] = array(
    '#type' => 'select',
    '#title' => t('Display in teaser and/or full view'),
    '#default_value' => variable_get('shoppingads_teaser_full_view', 0),
    '#options' => array(0 => t('Full view'), 1 => t('Teaser'), 2 => t('Teasers and full view')),
    '#description' => t('When to display shoppingads.'),
  );
  
  $form['shoppingads']['shoppingads_node_types'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Node Types'),
    '#default_value' => variable_get('shoppingads_node_types', array()),
    '#options' => node_get_types('names'),
    '#description' => t('Activate the node types where shoppingads.com advertising shall be displayed.')
  );

    // Ad keywords
  $form['shoppingads_kw'] = array(
    '#type' => 'fieldset',
    '#title' => t('Ad Keywords'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE
  );
  
    $form['shoppingads_kw']['shoppingads_keywords'] = array(
    '#type' => 'textfield',
    '#title' => t('Ad Keywords'),
    '#default_value' => variable_get('shoppingads_keywords', ''),
    '#size' => 60,
    '#maxlength' => 128,
    '#required' => TRUE,
    '#description' => t('Enter up to 15 keywords separated by semicolons ";". The keywords determine what kind of ads are displayed is as few as possible.')
  );

  if (module_exists('taxonomy')) {  
    $form['shoppingads_kw']['shoppingads_use_terms_as_keywords'] = array(
      '#type' => 'checkbox',
      '#title' => t('Use terms as keywords'),
      '#default_value' => variable_get('shoppingads_use_terms_as_keywords', ''),
      '#description' => t('By activating this option the terms/tags associated with the node will be used as keywords. If no terms are associated with the displayed node the keywords entered above will be used. For this feature the taxonomy module must be enabled.')
    );
    $v_options = shoppingads_get_vocabularies_options();
    $form['shoppingads_kw']['shoppingads_restrict_vocabulary'] = array(
      '#type' => 'select',
      '#title' => t('Restrict to vocabulary'),
      '#options' => $v_options,
      '#default_value' => variable_get('shoppingads_restrict_vocabulary', 0),
      '#description' => t('If you activated the <strong>Use terms as keywords</strong> option you can restrict the terms used as Ad search words to one vocabulary.')
    );
    $form['shoppingads_kw']['shoppingads_term_limit'] = array(
      '#type' => 'select',
      '#title' => t('Number of terms'),
      '#default_value' => variable_get('shoppingads_term_limit', 2),
      '#options' => drupal_map_assoc(range(1, 15)),
      '#description' => t('If you activated the <strong>Use terms as keywords</strong> option you can limit the number of terms to be used as search ad words here. 15 is the maximum number currently supported by shoppingads.')
    );
  }
  
  // Ad colors
  $form['shoppingads_colors'] = array(
    '#type' => 'fieldset',
    '#title' => t('Ad Colors'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE
  );
  foreach (shoppingads_ad_colors() as $area => $color) {
    $title = ucwords(str_replace('_', ' ', $area));
    $form['shoppingads_colors'][$area] = array(
      '#type' => 'textfield',
      '#title' => $title,
      '#default_value' => variable_get($area, $color),
      '#size' => 6,
      '#maxlength' => 6,
      '#description' => t('Enter a hexadecimal color value here, e.g. <code>ff9900</code>.')
    );
  }
  return system_settings_form($form);
}